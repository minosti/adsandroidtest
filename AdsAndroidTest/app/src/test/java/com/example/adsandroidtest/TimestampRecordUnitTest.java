package com.example.adsandroidtest;

import com.example.adsandroidtest.model.TimestampRecord;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class TimestampRecordUnitTest {

    TimestampRecord timestampRecord;

    @Before
    public void createTimestampRecord(){
        System.out.println("@Before createTimestampRecord");
        timestampRecord = new TimestampRecord();
    }

    @Test
    public void addTimestamp (){
        System.out.println("@Test addTimestamp");
        String strTimestamp = String.valueOf(System.currentTimeMillis());
        timestampRecord.addTimestampRecord(strTimestamp);
        assertTrue(timestampRecord.getTimestampRecords().contains(strTimestamp));

        timestampRecord.addTimestampRecord(null);
        assertEquals(1, timestampRecord.getTimestampRecords().size());
    }

    @Test
    public void getFormatRecord(){
        assertNotNull(timestampRecord.getFormatRecord());
        timestampRecord.addTimestampRecord(String.valueOf(System.currentTimeMillis()));
        assertNotNull(timestampRecord.getFormatRecord());
    }


}
