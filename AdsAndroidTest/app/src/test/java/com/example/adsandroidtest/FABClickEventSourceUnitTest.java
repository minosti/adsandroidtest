package com.example.adsandroidtest;

import com.example.adsandroidtest.model.FABClickEventSource;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class FABClickEventSourceUnitTest {

    TestObserver testObserver;
    FABClickEventSource fABClickEventSource;

    private static class TestObserver implements FABClickEventSource.ObserverCallBack {
        boolean hasBeenCall = false;
        @Override
        public void update() {
            hasBeenCall = true;
        }
    }

    @Before
    public void createSubject() {
        System.out.println("@Before createSubject");
        this.fABClickEventSource = new FABClickEventSource();
    }
    @Before
    public void createObserver() {
        System.out.println("@Before createObserver");
        this.testObserver = new TestObserver();
    }

    @Test
    public void registerObserverInSubject() {
        System.out.println("@Test registerObserverInSubject");
        fABClickEventSource.registerObserverCallBack(testObserver);
        assertTrue(fABClickEventSource.getTimestampListeners().contains(testObserver));

        fABClickEventSource.registerObserverCallBack(null);
        assertEquals(1,fABClickEventSource.getTimestampListeners().size());

    }

    @Test
    public void notifyObserver() {
        System.out.println("@Test notifyObserver");
        registerObserverInSubject();
        fABClickEventSource.notifyObserverCallBackList();
        assertTrue(testObserver.hasBeenCall);
    }

    @Test
    public void unregisterObserverInSubject() {
        System.out.println("@Test unregisterObserverInSubject");
        registerObserverInSubject();
        fABClickEventSource.unregisterObserverCallBack(testObserver);
        assertFalse(fABClickEventSource.getTimestampListeners().contains(testObserver));
    }

}