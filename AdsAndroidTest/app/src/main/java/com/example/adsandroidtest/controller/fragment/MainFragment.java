package com.example.adsandroidtest.controller.fragment;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.adsandroidtest.R;
import com.example.adsandroidtest.model.TimestampRecord;
import com.example.adsandroidtest.model.FABClickEventSource;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

    public class MainFragment extends Fragment implements FABClickEventSource.ObserverCallBack {

    private TextView textView;

    private FABClickEventSource fABClickEventSource;
    private final TimestampRecord timestampRecord;

    public MainFragment() {
        timestampRecord = new TimestampRecord();
    }

    public static MainFragment newInstance( FABClickEventSource fABClickEventSource) {
        MainFragment fragment = new MainFragment();
        if (fABClickEventSource != null) {
            fragment.fABClickEventSource = fABClickEventSource;
            // Register fragment as a ObserverCallBack in FABClickEventSource
            fABClickEventSource.registerObserverCallBack(fragment);
        }
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View mainFragmentView = inflater.inflate(R.layout.fragment_main, container, false);

        this.textView = mainFragmentView.findViewById(R.id.fragment_main_text_view);

        FloatingActionButton floatingActionButton = mainFragmentView.findViewById(R.id.fragment_main_fab);
        floatingActionButton.setOnClickListener(v -> {
            if(fABClickEventSource != null){
                fABClickEventSource.notifyObserverCallBackList();
            }
        });

        showFragment();

        return mainFragmentView;
    }

    // Create and add Fragment
    private void showFragment(){
       getChildFragmentManager().beginTransaction()
                .replace(R.id.fragment_main_second_frame_layout, ChildFragment.newInstance(fABClickEventSource))
                .commit();
    }

    @Override
    public void update() {
        // Add Timestamp
        timestampRecord.addTimestampRecord(String.valueOf(System.currentTimeMillis()));
        // Display Timestamp
        textView.setText(timestampRecord.getFormatRecord());
    }
}