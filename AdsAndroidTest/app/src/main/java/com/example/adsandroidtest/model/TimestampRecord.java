package com.example.adsandroidtest.model;

import java.util.ArrayList;
import java.util.List;

public class TimestampRecord {

    /** class to store strings (timestamps) and build structured string to display in TextView */

    // Defining String list
    private final List<String> timestampRecords = new ArrayList<>();

    public List<String> getTimestampRecords(){
        return timestampRecords;
    }

    // Return structured String based on timestampRecords list
    public String getFormatRecord(){
        StringBuilder formStrBuilder = new StringBuilder();
        for(int i=0;i<timestampRecords.size();i++){
            formStrBuilder
                    .append(i+1)
                    .append(" - ")
                    .append(timestampRecords.get(i))
                    .append(System.getProperty("line.separator"));
        }
        return formStrBuilder.toString();
    }

    // Add string (timestamp) in the list
    public void addTimestampRecord(String timestampRecord){
        if (timestampRecord != null) {
            timestampRecords.add(timestampRecord);
        }
    }
}
