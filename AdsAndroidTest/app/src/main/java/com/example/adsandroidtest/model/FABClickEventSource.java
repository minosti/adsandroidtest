package com.example.adsandroidtest.model;

import java.util.ArrayList;
import java.util.List;

public class FABClickEventSource {

    /**
     *  FABClickEventSource class is a 'Subject' in an Observer pattern
     *  it registers interface instances (ObserverCallBack) in a list
     *  and notify all the instances in list when notifyObserverCallBackList is called
     */

    // Defining interface
    public interface ObserverCallBack {
        void update();
    }

    // Defining interface instances list
    private final List<ObserverCallBack> observerCallBackList = new ArrayList<>();

    public List<ObserverCallBack> getTimestampListeners(){
        return observerCallBackList;
    }

    // Register interface ObserverCallBack instance in list
    public void registerObserverCallBack(ObserverCallBack observerCallBack){
        if (observerCallBack != null && !observerCallBackList.contains(observerCallBack)) {
            observerCallBackList.add(observerCallBack);
        }
    }

    // Notify the instances in the list by calling their inherited (update()) method
    public void notifyObserverCallBackList(){
        for(final ObserverCallBack observerCallBack : observerCallBackList){
            observerCallBack.update();
        }
    }

    // Unregister Interface ObserverCallBack instance in list
    public void unregisterObserverCallBack(ObserverCallBack observerCallBack){
        observerCallBackList.remove(observerCallBack);
    }
}