package com.example.adsandroidtest.controller.fragment;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.adsandroidtest.R;
import com.example.adsandroidtest.controller.view.ChildFragmentView;
import com.example.adsandroidtest.model.TimestampRecord;
import com.example.adsandroidtest.model.FABClickEventSource;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class ChildFragment extends Fragment implements FABClickEventSource.ObserverCallBack {

    private TextView textView;

    private FABClickEventSource fABClickEventSource;
    private final TimestampRecord timestampRecord;

    public ChildFragment() {
        timestampRecord = new TimestampRecord();
    }

    public static ChildFragment newInstance(FABClickEventSource fABClickEventSource) {
        ChildFragment fragment = new ChildFragment();
        if (fABClickEventSource != null) {
            fragment.fABClickEventSource = fABClickEventSource;
            // Register fragment as a ObserverCallBack in FABClickEventSource
            fABClickEventSource.registerObserverCallBack(fragment);
        }
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_child, container, false);

        this.textView = view.findViewById(R.id.fragment_child_text_view);

        FloatingActionButton floatingActionButton = view.findViewById(R.id.fragment_child_fab);
        floatingActionButton.setOnClickListener(v -> {
            if(fABClickEventSource != null){
                fABClickEventSource.notifyObserverCallBackList();
            }
        });

        // Create ChildFragmentView
        ChildFragmentView childFragmentView = ChildFragmentView.newInstance(getContext(),fABClickEventSource);
        childFragmentView.setLayoutParams(
                new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.MATCH_PARENT
                )
        );

        LinearLayout linearLayout = view.findViewById(R.id.fragment_child_linear_layout);
        // Add ChildFragmentView
        linearLayout.addView(childFragmentView);

        return view;
    }

    @Override
    public void update() {
        // Add Timestamp
        timestampRecord.addTimestampRecord(String.valueOf(System.currentTimeMillis()));
        // Display Timestamp
        textView.setText(timestampRecord.getFormatRecord());
    }
}