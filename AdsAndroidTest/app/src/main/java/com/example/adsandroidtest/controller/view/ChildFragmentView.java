package com.example.adsandroidtest.controller.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.example.adsandroidtest.R;
import com.example.adsandroidtest.model.TimestampRecord;
import com.example.adsandroidtest.model.FABClickEventSource;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class ChildFragmentView extends FrameLayout implements FABClickEventSource.ObserverCallBack {

    private final TimestampRecord timestampRecord;
    private FABClickEventSource fABClickEventSource;

    private final TextView textView;

    public static ChildFragmentView newInstance(Context context, FABClickEventSource fABClickEventSource){
        ChildFragmentView childFragmentView = new ChildFragmentView(context);

        if (fABClickEventSource != null) {
            fABClickEventSource.registerObserverCallBack(childFragmentView);
            // Register View as a ObserverCallBack in FABClickEventSource
            childFragmentView.fABClickEventSource = fABClickEventSource;
        }
        return childFragmentView;
    }

    public ChildFragmentView(Context context) {
        super(context);
        LayoutInflater.from(context).inflate(R.layout.view_child_fragment, this);

        this.textView = findViewById(R.id.view_child_text_view);

        FloatingActionButton floatingActionButton = findViewById(R.id.view_child_fab);
        floatingActionButton.setOnClickListener(v -> {
            if(fABClickEventSource != null){
                fABClickEventSource.notifyObserverCallBackList();
            }
        });

        this.timestampRecord = new TimestampRecord();
    }

    @Override
    public void update() {
        // Add Timestamp
        timestampRecord.addTimestampRecord(String.valueOf(System.currentTimeMillis()));
        // Display Timestamp
        textView.setText(timestampRecord.getFormatRecord());
    }
}
