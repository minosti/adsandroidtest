package com.example.adsandroidtest.controller.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.example.adsandroidtest.R;
import com.example.adsandroidtest.model.FABClickEventSource;
import com.example.adsandroidtest.model.TimestampRecord;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class DrawerView extends FrameLayout implements FABClickEventSource.ObserverCallBack {

    private final TimestampRecord timestampRecord;
    private FABClickEventSource fABClickEventSource;

    private final TextView textView;

    public static DrawerView newInstance(Context context, FABClickEventSource fABClickEventSource){
        DrawerView drawerView = new DrawerView(context);

        if (fABClickEventSource != null) {
            fABClickEventSource.registerObserverCallBack(drawerView);
            // Register View as a ObserverCallBack in FABClickEventSource
            drawerView.fABClickEventSource = fABClickEventSource;
        }
        return drawerView;
    }

    public DrawerView(Context context) {
        super(context);
        LayoutInflater.from(context).inflate(R.layout.view_drawer, this);

        this.textView = findViewById(R.id.view_drawer_text_view);

        FloatingActionButton floatingActionButton = findViewById(R.id.view_drawer_fab);
        floatingActionButton.setOnClickListener(v -> {
            if(fABClickEventSource != null){
                fABClickEventSource.notifyObserverCallBackList();
            }
        });

        this.timestampRecord = new TimestampRecord();
    }

    @Override
    public void update() {
        // Add Timestamp
        timestampRecord.addTimestampRecord(String.valueOf(System.currentTimeMillis()));
        // Display Timestamp
        textView.setText(timestampRecord.getFormatRecord());
    }
}