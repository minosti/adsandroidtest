package com.example.adsandroidtest.controller.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.LinearLayout;

import com.example.adsandroidtest.R;
import com.example.adsandroidtest.controller.view.DrawerView;
import com.example.adsandroidtest.controller.fragment.MainFragment;
import com.example.adsandroidtest.model.FABClickEventSource;

public class MainActivity extends AppCompatActivity  {

    private FABClickEventSource fABClickEventSource;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Create Event Source
        this.fABClickEventSource = new FABClickEventSource();

        setupNavigationView();
        showFragment();
    }

    // Create and add DrawerView
    private void setupNavigationView(){
        LinearLayout linearLayout = findViewById(R.id.activity_main_nav_view);
        DrawerView drawerView = DrawerView.newInstance(getBaseContext(),fABClickEventSource);
        drawerView.setLayoutParams(
                new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.MATCH_PARENT
                )
        );
        linearLayout.addView(drawerView);
    }

    // Create and add Fragments
    private void showFragment(){
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.activity_main_first_frame_layout, MainFragment.newInstance(fABClickEventSource))
                .replace(R.id.activity_main_second_frame_layout, MainFragment.newInstance(fABClickEventSource))
                .commit();
    }

}